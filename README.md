# Terraform API
An HTTP API interface for Terraform CLI - single user, single project, single deployment.

## Table of Contents
- [Introduction](#introduction)
- [Prerequisites](#prerequisites)
- [Installation and Quickstart](#running)
- [Development](#development)
- [Usage](#usage)
- [License](#license)
- [Contact](#contact)
- [Acknowledgement](#acknowledgement)

## Introduction
This project contains an API to a Terraform exec library for management of a single deployment Terraform project.
It can be used to manage your single Terraform deployment environment inside a container.

## Running and usage
This part explains how to run the Terraform API.
After that you will be able to call different API endpoints that correspond to Terrafrom CLI.
You can also use the API as [terraform-lcm-service-api](https://pkg.go.dev/gitlab.com/gaia-x/data-infrastructure-federation-services/orc/lcm-service/terraform-lcm-service-api)
GoLang package.

### Run in Docker
TBD

### Run locally from source
Install [go](https://go.dev/doc/install).
After that use the following commands:

```console
git clone git@gitlab.com:gaia-x/data-infrastructure-federation-services/orc/lcm-service/terraform-lcm-service-api.git
cd terraform-lcm-service-api
go mod download
swag init
go run main.go
```

## License
TBD

## Contact
TBD

## Acknowledgement
TBD
