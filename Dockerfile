FROM golang:alpine3.15 AS build

# set workdir and copy files
WORKDIR /terraform-api
COPY . ./

# install packages, generate API docs and build the API
RUN apk update && apk add --no-cache git \
    && go mod download \
    && go install github.com/swaggo/swag/cmd/swag@latest \
    && $GOPATH/bin/swag init --parseDependency --parseInternal --parseDepth 1 \
    && CGO_ENABLED=0 GOOS=linux go build -v -a -installsuffix cgo -o main main.go

FROM golang:alpine3.15

# set workdir and copy files from build
WORKDIR /terraform-api
COPY --from=build ./terraform-api/main .

# expose API port
EXPOSE 8080

# prepare API for running
CMD ["./main"]
