package main

import (
	"context"
	"fmt"
	"os"
	"strconv"

	log "github.com/sirupsen/logrus"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/orc/lcm-service/terraform-lcm-service-api/api"
	_ "gitlab.com/gaia-x/data-infrastructure-federation-services/orc/lcm-service/terraform-lcm-service-api/docs"
)

// @version 0.1.2
// @description A stateful Terraform API for orchestration environment (single user, single project, single deployment)
// @license.name Mozilla Public License 2.0
// @license.url https://www.mozilla.org/en-US/MPL/2.0/

var TerraformVersion = "1.1.8"
var WorkingDirectory = ""
var ApiDebugMode = false
var ApiPort = "8080"
var TrustedProxy = ""

// getCurrentWorkingDirectory retrieves current workdir
func getCurrentWorkingDirectory() string {
	workdir, err := os.Getwd()
	if err != nil {
		log.Fatal(fmt.Sprintf("error getting working directory: %s", err))
	}

	return workdir
}

// setVars sets vars by reading environment variables
// TODO: use cli flags instead of env vars for this purpose
func setVars() {
	// get TERRAFORM_API_TERRAFORM_VERSION env var if set
	value, ok := os.LookupEnv("TERRAFORM_API_TERRAFORM_VERSION")
	if ok {
		TerraformVersion = value
	}

	// get TERRAFORM_API_WORKDIR env var if set
	value, ok = os.LookupEnv("TERRAFORM_API_WORKDIR")
	if ok {
		WorkingDirectory = value
		dir, err := os.Stat(value)
		if err != nil {
			log.Error(fmt.Sprintf("failed to open directory %s, error: %s", WorkingDirectory, err))
			WorkingDirectory = getCurrentWorkingDirectory()
		} else if !dir.IsDir() {
			log.Error(fmt.Sprintf("%q is not a directory", dir.Name()))
			WorkingDirectory = getCurrentWorkingDirectory()
		}
	} else {
		WorkingDirectory = getCurrentWorkingDirectory()
	}

	// get TERRAFORM_API_DEBUG_MODE env var if set
	value, ok = os.LookupEnv("TERRAFORM_API_DEBUG_MODE")
	if ok {
		valueBool, err := strconv.ParseBool(value)
		if err != nil {
			log.Error(fmt.Sprintf("failed to convert value %s to bool, error: %s", value, err))
		} else {
			ApiDebugMode = valueBool
		}
	}

	// get TERRAFORM_API_TRUSTED_PROXY env var if set
	value, ok = os.LookupEnv("TERRAFORM_API_TRUSTED_PROXY")
	if ok {
		TrustedProxy = value
	}

	// get TERRAFORM_API_PORT env var if set
	value, ok = os.LookupEnv("TERRAFORM_API_PORT")
	if ok {
		ApiPort = value
	}
}

// main configures and starts the API
func main() {
	// init vars from environment
	setVars()
	// init Terraform
	api.Terraform = api.InitTerraform(context.Background(), TerraformVersion, WorkingDirectory)
	// init API router
	router := api.SetupRouter(ApiDebugMode, TrustedProxy)
	// run the API
	err := router.Run(":" + ApiPort)
	if err != nil {
		log.Fatal(fmt.Sprintf("error starting Terraform API: %s", err))
	}
}
