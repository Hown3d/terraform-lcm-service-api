local-build:
	KO_DOCKER_REPO=ko.local ko build .

generate-swagger:
	go install github.com/swaggo/swag/cmd/swag@latest
	$(shell go env GOPATH)/bin/swag init