package api

import (
	"encoding/json"

	"github.com/hashicorp/go-version"
	"github.com/hashicorp/terraform-exec/tfexec"
	tfjson "github.com/hashicorp/terraform-json"
)

// JSONResult represents API JSON response
type JSONResult struct {
	Message string          `json:"message"`
	Data    json.RawMessage `json:"data"`
}

// ApplyQueryParams represents query params for /apply handle
type ApplyQueryParams struct {
	RefreshOnly bool   `form:"refresh_only"`
	Replace     string `form:"replace"`
}

// ForceUnlockQueryParams represents query params for /force-unlock handle
type ForceUnlockQueryParams struct {
	LockId string `form:"lock_id"`
}

// ImportQueryParams represents query params for /import handle
type ImportQueryParams struct {
	Address string `form:"address"`
	Id      string `form:"id"`
}

// StateRmQueryParams represents query params for /state/rm handle
type StateRmQueryParams struct {
	Address string `form:"address"`
}

// StateMvQueryParams represents query params for /state/mv handle
type StateMvQueryParams struct {
	Source      string `form:"source"`
	Destination string `form:"destination"`
}

// UntaintQueryParams represents query params for /untaint handle
type UntaintQueryParams struct {
	Address string `form:"address"`
}

// WorkspaceSelectQueryParams represents query params for /workspace/select handle
type WorkspaceSelectQueryParams struct {
	Name string `form:"name"`
}

// WorkspaceNewQueryParams represents query params for /workspace/new handle
type WorkspaceNewQueryParams struct {
	Name string `form:"name"`
}

// WorkspaceDeleteQueryParams represents query params for /workspace/delete handle
type WorkspaceDeleteQueryParams struct {
	Name string `form:"name"`
}

type TerraformVersionOutput struct {
	Version   string            `json:"version,omitempty"`
	Providers map[string]string `json:"providers,omitempty"`
}

func newTerraformVersionOutput(tfVersion *version.Version, providerVersions map[string]*version.Version) TerraformVersionOutput {
	providers := make(map[string]string, len(providerVersions))
	for name, version := range providerVersions {
		providers[name] = version.String()
	}
	return TerraformVersionOutput{
		Version:   tfVersion.String(),
		Providers: providers,
	}
}

type TerraformWorkspaceListOutput struct {
	Current string   `json:"current,omitempty"`
	List    []string `json:"list,omitempty"`
}

type TerraformGraphOutput string

type TerraformOutputOutput struct {
	Output map[string]tfexec.OutputMeta `json:"output,omitempty"`
}

type TerraformPlanOutput bool

type TerraformValidateOutput struct {
	*tfjson.ValidateOutput
}

type TerraformFormatOutput struct {
	Formatted    bool     `json:"formatted,omitempty"`
	ChangedFiles []string `json:"changed_files,omitempty"`
}

type TerraformProvidersSchemaOutput struct {
	*tfjson.ProviderSchemas
}

type TerraformShowOutput struct {
	*tfjson.State
}

type TerraformWorkspaceOutput string