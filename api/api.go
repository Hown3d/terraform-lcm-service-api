package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/hashicorp/go-version"
	"github.com/hashicorp/hc-install/product"
	"github.com/hashicorp/hc-install/releases"
	"github.com/hashicorp/terraform-exec/tfexec"
	log "github.com/sirupsen/logrus"
)

// Terraform contains an object for executing commands
// TODO: dont use global variable, encapsulate into struct
// TODO: move outside from api package
var Terraform *tfexec.Terraform

// InitTerraform installs Terraform and initializes Terraform struct object
// TODO: move outside from api package
func InitTerraform(ctx context.Context, terraformVersion, workingDir string) *tfexec.Terraform {
	installer := &releases.ExactVersion{
		Product: product.Terraform,
		Version: version.Must(version.NewVersion(terraformVersion)),
	}

	execPath, err := installer.Install(ctx)
	if err != nil {
		log.Fatal(fmt.Errorf("error installing Terraform: %w", err))
	}

	tf, err := tfexec.NewTerraform(workingDir, execPath)
	if err != nil {
		log.Fatal(fmt.Errorf("error running NewTerraform: %w", err))
	}

	// TODO: maybe attach stdout and stderr to an io.Writer to capture the outputs
	// (e.g. terraform apply, terraform plan, terraform destroy output)
	// e.g:
	// var outBuf *bytes.Buffer
	// var errBuf *bytes.Buffer
	// tf.SetStdout(outBuf)
	// tf.SetStderr(errBuf)

	return tf
}

// Init calls terraform init command
// @Summary Prepares your working directory for other commands
// @Description Initializes a working directory containing Terraform configuration files
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /init [post]
func initHandler(ctx *gin.Context) {
	// TODO: add possibility to run Update/Upgrade by specifying a query param (e.g. update=true)
	err := Terraform.Init(ctx, tfexec.Upgrade(true))
	if err != nil {
		err = fmt.Errorf("error running init: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, JSONResult{Message: "Terraform has been successfully initialized!"})
}

// Validate calls terraform validate command
// @Summary Checks whether the configuration is valid
// @Description Validates the configuration files in a directory
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /validate [post]
func validateHandler(ctx *gin.Context) {
	state, err := Terraform.Validate(ctx)
	if err != nil {
		err = fmt.Errorf("error running validate: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	data, err := json.Marshal(TerraformValidateOutput{state})
	if err != nil {
		jsonMarshalError(ctx, err)
		return
	}
	// TODO: not sure if the message makes sense here:
	// validate will return err == nil if the configuration is not valid too,
	// since `terraform validate -json` will return with exitcode 0 if there are configuration errors.
	ctx.JSON(http.StatusOK, JSONResult{Message: "Success! The configuration is valid.", Data: data})
}

// Plan calls terraform plan command
// @Summary Shows changes required by the current configuration
// @Description Creates an execution plan, which lets you preview the changes for your infrastructure
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /plan [post]
func planHandler(ctx *gin.Context) {
	changed, err := Terraform.Plan(ctx)
	if err != nil {
		err = fmt.Errorf("error running plan: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	data, err := json.Marshal(TerraformPlanOutput(changed))
	if err != nil {
		jsonMarshalError(ctx, err)
		return
	}
	ctx.JSON(http.StatusOK, JSONResult{Data: data})
}

// Apply calls terraform apply command
// @Summary Create or update infrastructure
// @Description Executes the actions proposed in a Terraform plan
// @Param refresh_only query bool false "If true updates the state to match remote systems"
// @Param replace query string false "An address of the resource to be marked as tainted (degraded or damaged object)"
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /apply [post]
func applyHandler(ctx *gin.Context) {
	var queryParams ApplyQueryParams
	err := ctx.ShouldBindQuery(&queryParams)
	if err != nil {
		queryError(ctx, err)
		return
	}

	applyOpts := []tfexec.ApplyOption{}
	applyOpts = append(applyOpts, tfexec.Refresh(queryParams.RefreshOnly))
	if queryParams.Replace != "" {
		applyOpts = append(applyOpts, tfexec.Replace(queryParams.Replace))
	}

	err = Terraform.Apply(ctx, applyOpts...)
	if err != nil {
		err = fmt.Errorf("error running apply: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, JSONResult{Message: "Apply complete!"})
}

// Destroy calls terraform destroy command
// @Summary Destroys previously-created infrastructure
// @Description Destroys all remote objects managed by a particular Terraform configuration
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /destroy [post]
func destroyHandler(ctx *gin.Context) {
	err := Terraform.Destroy(ctx)
	if err != nil {
		err = fmt.Errorf("error running destroy: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, JSONResult{Message: "Destroy complete!"})
}

// Fmt calls terraform fmt command
// @Summary Reformats your configuration in the standard style
// @Description Rewrites Terraform configuration files to a canonical format and style
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /fmt [post]
func fmtHandler(ctx *gin.Context) {
	formatted, changedFiles, err := Terraform.FormatCheck(ctx, tfexec.Recursive(true))
	if err != nil {
		err = fmt.Errorf("error running fmt: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	out := TerraformFormatOutput{Formatted: formatted, ChangedFiles: changedFiles}
	data, err := json.Marshal(out)
	if err != nil {
		jsonMarshalError(ctx, err)
		return
	}
	if formatted {
		ctx.JSON(http.StatusOK, JSONResult{Message: "config files are already formatted!", Data: data})
		return
	}

	err = Terraform.FormatWrite(ctx, tfexec.Recursive(true))
	if err != nil {
		err = fmt.Errorf("error running fmt: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, JSONResult{Message: "config files got formatted", Data: data})
}

// ForceUnlock calls terraform force-unlock command
// @Summary Releases a stuck lock on the current workspace
// @Description Manually unlocks the state for the defined configuration
// @Param lock_id query string true "A unique lock id"
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /force-unlock [post]
func forceUnlockHandler(ctx *gin.Context) {
	var queryParams ForceUnlockQueryParams
	err := ctx.ShouldBindQuery(&queryParams)
	if err != nil {
		queryError(ctx, err)
		return
	}

	err = Terraform.ForceUnlock(ctx, queryParams.LockId)
	if err != nil {
		err = fmt.Errorf("error running force-unlock: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, JSONResult{Message: "Terraform state has been successfully unlocked!"})
}

// Get calls terraform get command
// @Summary Installs or upgrades remote Terraform modules
// @Description Destroys all remote objects managed by a particular Terraform configuration
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /get [post]
func getHandler(ctx *gin.Context) {
	// TODO: add possibility to run Update/Upgrade by specifying a query param (e.g. update=true)
	err := Terraform.Get(ctx, tfexec.Update(true))
	if err != nil {
		err = fmt.Errorf("error running get: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, nil)
}

// Graph calls terraform graph command
// @Summary Generates a Graphviz graph of the steps in an operation
// @Description Generates a visual representation of either a configuration or execution plan
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /graph [post]
func graphHandler(ctx *gin.Context) {
	graph, err := Terraform.Graph(ctx)
	if err != nil {
		err = fmt.Errorf("error running graph: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	data, err := json.Marshal(TerraformGraphOutput(graph))
	ctx.JSON(http.StatusOK, JSONResult{Data: data})
}

// Import calls terraform import command
// @Summary Associates existing infrastructure with a Terraform resource
// @Description Imports existing resources into Terraform
// @Param address query string true "A valid resource address at which resource will be imported"
// @Param id query string true "An existing resource id that will be found by import"
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /import [post]
func importHandler(ctx *gin.Context) {
	var queryParams ImportQueryParams
	err := ctx.ShouldBindQuery(&queryParams)
	if err != nil {
		queryError(ctx, err)
		return
	}

	err = Terraform.Import(ctx, queryParams.Address, queryParams.Id)
	if err != nil {
		err = fmt.Errorf("error running import: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, JSONResult{Message: "Import successful!"})
}

// Output calls terraform output command
// @Summary Shows output values from your root module
// @Description Extracts the value of an output variable from the state file
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /output [get]
func outputHandler(ctx *gin.Context) {
	output, err := Terraform.Output(ctx)
	if err != nil {
		err = fmt.Errorf("error running output: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	out := TerraformOutputOutput{
		Output: output,
	}
	data, err := json.Marshal(out)
	if err != nil {
		jsonMarshalError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, JSONResult{Data: data})
}

// ProvidersSchema calls terraform providers schema command
// @Summary Shows the providers required for this configuration
// @Description Prints detailed schemas for the providers used in the current configuration
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /providers/schema [get]
func providersSchemaHandler(ctx *gin.Context) {
	providersSchema, err := Terraform.ProvidersSchema(ctx)
	if err != nil {
		err = fmt.Errorf("error running providers schema: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	out := TerraformProvidersSchemaOutput{providersSchema}
	data, err := json.Marshal(out)
	if err != nil {
		jsonMarshalError(ctx, err)
		return
	}
	ctx.JSON(http.StatusOK, JSONResult{Data: data})
}

// ProvidersLock calls terraform providers lock command
// @Summary Updates the dependency lock file to include a selected version for each provider
// @Description Consults upstream registries to write provider dependency information into the dependency lock file
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /providers/lock [post]
func providersLockHandler(ctx *gin.Context) {
	err := Terraform.ProvidersLock(ctx)
	if err != nil {
		err = fmt.Errorf("error running providers lock: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, JSONResult{Message: "Success! Terraform has updated the lock file."})
}

// Show calls terraform show command
// @Summary Shows the current state or a saved plan
// @Description Provides human-readable output from a state or plan file
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /show [get]
func showHandler(ctx *gin.Context) {
	state, err := Terraform.Show(ctx)
	if err != nil {
		err = fmt.Errorf("error running show: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	out := TerraformShowOutput{state}
	data, err := json.Marshal(out)
	if err != nil {
		jsonMarshalError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, JSONResult{Data: data})
}

// StateRm calls terraform state rm command
// @Summary Forgets the resource, while it continues to exist in the remote system
// @Description Removes a binding to an existing remote object without first destroying it
// @Param address query string true "A valid resource address to be removed from record"
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /state/rm [delete]
func stateRmHandler(ctx *gin.Context) {
	var queryParams StateRmQueryParams
	err := ctx.ShouldBindQuery(&queryParams)
	if err != nil {
		queryError(ctx, err)
		return
	}

	err = Terraform.StateRm(ctx, queryParams.Address)
	if err != nil {
		err = fmt.Errorf("error running state rm: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, nil)
}

// StateMv calls terraform state mv command
// @Summary Moves the remote objects currently associated with the source to be tracked instead by the destination
// @Description Retains an existing remote object but track it as a different resource instance address
// @Param source query string true "A valid resource address for source"
// @Param destination query string true "A valid resource address for destination"
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /state/mv [post]
func stateMvHandler(ctx *gin.Context) {
	var queryParams StateMvQueryParams
	err := ctx.ShouldBindQuery(&queryParams)
	if err != nil {
		queryError(ctx, err)
		return
	}

	err = Terraform.StateMv(ctx, queryParams.Source, queryParams.Destination)
	if err != nil {
		err = fmt.Errorf("error running state mv: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, nil)
}

// Untaint calls terraform untaint command
// @Summary Removes the tainted state from a resource instance
// @Description Removes the taint marker from the object (will not modify remote objects, will modify the state)
// @Param address query string true "A resource address for particular resource instance which is currently tainted"
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /untaint [delete]
func untaintHandler(ctx *gin.Context) {
	var queryParams UntaintQueryParams
	err := ctx.ShouldBindQuery(&queryParams)
	if err != nil {
		queryError(ctx, err)
		return
	}

	err = Terraform.Untaint(ctx, queryParams.Address)
	if err != nil {
		err = fmt.Errorf("error running untaint: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, nil)
}

// Version calls terraform version command
// @Summary Shows the current Terraform version
// @Description Displays the current version of Terraform and all installed plugins
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /version [get]
func versionHandler(ctx *gin.Context) {
	terraformVersion, providersVersion, err := Terraform.Version(ctx, true)
	if err != nil {
		err = fmt.Errorf("error running untaint: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	out := newTerraformVersionOutput(terraformVersion, providersVersion)
	data, err := json.Marshal(out)
	if err != nil {
		jsonMarshalError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, JSONResult{Data: data})
}

// WorkspaceShow calls terraform workspace show command
// @Summary Shows the name of the current workspace
// @Description Outputs the current Terraform workspace
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /workspace/show [get]
func workspaceShowHandler(ctx *gin.Context) {
	currentWorkspace, err := Terraform.WorkspaceShow(ctx)
	if err != nil {
		err = fmt.Errorf("error running workspace show: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	out := TerraformWorkspaceOutput(currentWorkspace)
	data, err := json.Marshal(out)
	if err != nil {
		jsonMarshalError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, JSONResult{Data: data})
}

// WorkspaceList calls terraform workspace list command
// @Summary Lists Terraform workspaces
// @Description Lists all existing Terraform workspaces
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /workspace/list [get]
func workspaceListHandler(ctx *gin.Context) {
	workspaceList, currentWorkspace, err := Terraform.WorkspaceList(ctx)
	if err != nil {
		err = fmt.Errorf("error running workspace list: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	out := TerraformWorkspaceListOutput{
		Current: currentWorkspace,
		List:    workspaceList,
	}
	data, err := json.Marshal(out)
	if err != nil {
		jsonMarshalError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, JSONResult{Data: data})
}

// WorkspaceSelect calls terraform workspace select command
// @Summary Select a workspace
// @Description Chooses a different Terraform workspace to use for further operations
// @Param name query string true "A name of existing Terraform workspace"
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /workspace/select [post]
func workspaceSelectHandler(ctx *gin.Context) {
	var queryParams WorkspaceSelectQueryParams
	err := ctx.ShouldBindQuery(&queryParams)
	if err != nil {
		queryError(ctx, err)
		return
	}

	err = Terraform.WorkspaceSelect(ctx, queryParams.Name)
	if err != nil {
		err = fmt.Errorf("error running workspace select: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, nil)
}

// WorkspaceNew calls terraform workspace new command
// @Summary Creates a new workspace
// @Description Creates a new Terraform workspace with the given name
// @Param name query string true "A name of (unexisting) Terraform workspace to be created"
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /workspace/new [post]
func workspaceNewHandler(ctx *gin.Context) {
	var queryParams WorkspaceNewQueryParams
	err := ctx.ShouldBindQuery(&queryParams)
	if err != nil {
		queryError(ctx, err)
		return
	}

	err = Terraform.WorkspaceNew(ctx, queryParams.Name)
	if err != nil {
		err = fmt.Errorf("error running workspace new: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, nil)
}

// WorkspaceDelete calls terraform workspace delete command
// @Summary Deletes a workspace
// @Description Deletes an existing Terraform workspace
// @Param name query string true "A name of existing Terraform workspace to be deleted"
// @Produce json
// @Success 200 {object} JSONResult
// @Failure 500 {object} JSONResult
// @Router /workspace/delete [delete]
func workspaceDeleteHandler(ctx *gin.Context) {
	var queryParams WorkspaceDeleteQueryParams
	err := ctx.ShouldBindQuery(&queryParams)
	if err != nil {
		queryError(ctx, err)
		return
	}

	err = Terraform.WorkspaceDelete(ctx, queryParams.Name)
	if err != nil {
		err = fmt.Errorf("error running workspace delete: %w", err)
		abortCall(ctx, http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, nil)
}
