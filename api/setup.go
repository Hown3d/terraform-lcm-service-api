package api

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/gaia-x/data-infrastructure-federation-services/orc/lcm-service/terraform-lcm-service-api/docs"
)

// SetupRouter initializes an API router for production
func SetupRouter(debugMode bool, trustedProxy string) *gin.Engine {
	// init API router
	router := gin.Default()

	// set API routes
	router.POST("/init", initHandler)
	router.POST("/validate", validateHandler)
	router.POST("/plan", planHandler)
	router.POST("/apply", applyHandler)
	router.POST("/destroy", destroyHandler)
	router.POST("/fmt", fmtHandler)
	router.POST("/force-unlock", forceUnlockHandler)
	router.POST("/get", getHandler)
	router.POST("/graph", graphHandler)
	router.POST("/import", importHandler)
	router.GET("/output", outputHandler)
	router.GET("/providers/schema", providersSchemaHandler)
	router.POST("/providers/lock", providersLockHandler)
	router.GET("/show", showHandler)
	router.DELETE("/state/rm", stateRmHandler)
	router.POST("/state/mv", stateMvHandler)
	router.DELETE("/untaint", untaintHandler)
	router.GET("/version", versionHandler)
	router.GET("/workspace/show", workspaceShowHandler)
	router.GET("/workspace/list", workspaceListHandler)
	router.POST("/workspace/select", workspaceSelectHandler)
	router.POST("/workspace/new", workspaceNewHandler)
	router.DELETE("/workspace/delete", workspaceDeleteHandler)

	// serve API docs (Swagger UI) when in debug mode
	if debugMode {
		router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
		router.GET("swagger.json", func(c *gin.Context) {
			c.File("docs/swagger.json")
		})
		router.GET("swagger.yaml", func(c *gin.Context) {
			c.File("docs/swagger.yaml")
		})
		router.GET("/swagger", func(c *gin.Context) {
			c.Redirect(http.StatusMovedPermanently, "/swagger/index.html")
		})
	}

	// set trusted proxy if needed
	if trustedProxy != "" {
		err := router.SetTrustedProxies([]string{trustedProxy})
		if err != nil {
			log.Fatal(fmt.Sprintf("error setting trusted proxy: %s", err))
		}
	}

	return router
}
