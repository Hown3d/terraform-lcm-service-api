package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

var router *gin.Engine
var once sync.Once

func CommonTestCase(httpRequest *http.Request) (int, JSONResult) {
	once.Do(func() {
		// init Terraform (only first time)
		log.Debug("setup")
		Terraform = InitTerraform(context.Background(), "1.1.8", "../tests/hello_world")
		// setup router (only first time)
		gin.SetMode(gin.TestMode)
		router = SetupRouter(true, "")
	})

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, httpRequest)

	var jsonResult JSONResult
	err := json.Unmarshal(recorder.Body.Bytes(), &jsonResult)
	if err != nil {
		log.Fatal(fmt.Sprintf("Cannot unmarshal JSON: %s", err))
	}

	return recorder.Code, jsonResult
}

func TestInitRoute(t *testing.T) {
	req, _ := http.NewRequest("POST", "/init", nil)
	statusCode, jsonResult := CommonTestCase(req)

	assert.Equal(t, 200, statusCode)
	assert.Equal(t, "Terraform has been successfully initialized!", jsonResult.Message)
}

func TestValidateRoute(t *testing.T) {
	req, _ := http.NewRequest("POST", "/validate", nil)
	statusCode, jsonResult := CommonTestCase(req)

	assert.Equal(t, 200, statusCode)
	assert.Equal(t, "Success! The configuration is valid.", jsonResult.Message)
}

func TestPlanRoute(t *testing.T) {
	req, _ := http.NewRequest("POST", "/plan", nil)
	statusCode, _ := CommonTestCase(req)

	assert.Equal(t, 200, statusCode)
}

func TestApplyRoute(t *testing.T) {
	req, _ := http.NewRequest("POST", "/apply", nil)
	statusCode, jsonResult := CommonTestCase(req)

	assert.Equal(t, 200, statusCode)
	assert.Equal(t, "Apply complete!", jsonResult.Message)
}

func TestDestroyRoute(t *testing.T) {
	req, _ := http.NewRequest("POST", "/destroy", nil)
	statusCode, jsonResult := CommonTestCase(req)

	assert.Equal(t, 200, statusCode)
	assert.Equal(t, "Destroy complete!", jsonResult.Message)
}
