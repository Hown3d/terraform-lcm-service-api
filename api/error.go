package api

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

func abortCall(ctx *gin.Context, statuscode int, err error) {
	log.Error(err)
	ctx.AbortWithStatusJSON(http.StatusInternalServerError, JSONResult{Message: err.Error()})
}

func jsonMarshalError(ctx *gin.Context, err error) {
	err = fmt.Errorf("error building (marshal) JSON: %w", err)
	abortCall(ctx, http.StatusInternalServerError, err)
}

func queryError(ctx *gin.Context, err error) {
	err = fmt.Errorf("error binding query params: %w", err)
	abortCall(ctx, http.StatusInternalServerError, err)
}
